# About

- Set reminders
- Display reminders at a certain time
- Log meetings
- Add events to [LibreOrganize](https://gitlab.com/libreorganize/libreorganize/)
- Display the current (machine) time
- Access logs through any WSGI-compatible webserver (using [irclog2html](https://github.com/mgedmin/irclog2html))

# How to Use

- Clone this repository using `$ git clone git@gitlab.com:/novawebdevelopment/little_brother.git`.
- Change the current directory using `$ cd little_brother`.
- Create a virtual environment using `$ python3 -m venv venv`.
- Activate the virtual environment using `$ source venv/bin/activate`.
- Install the requirements using `$ pip install -r requirements.txt`.
- Change the IRC server and channel to your own in `config.ini`.
- Connect a webserver to `wsgi.py`.
- Start the bot using `./start`.

# Authors

- **Marco Sirabella** - *Initial Work* - [mjsir911](https://github.com/mjsir911/)
- **Adrian Buchholz** - *Events Integration* - [abuchholz](https://gitlab.com/abuchholz/)
- **Stefan-Ionut Tarabuta** - *Refactor and Setup* - [SITarabuta](https://gitlab.com/sitarabuta/)
