#!/usr/bin/env python3

import irc3
from irc3.plugins.logger import file_handler
from irc3.plugins.command import command
from irc3.plugins.cron import cron
import mechanize
import env_file
import os

import datetime
import asyncio

__appname__     = ""
__author__      = "Marco Sirabella"
__copyright__   = ""
__credits__     = ["Marco Sirabella",
                   "Adrian Buchholz",
                   "Stefan-Ionut Tarabuta",
                   "Kevin Cole"]
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = ["Marco Sirabella",
                   "Adrian Buchholz",
                   "Stefan-Ionut Tarabuta",
                   "Kevin Cole"]
__email__       = "admin@novawebdevelopment.org"
__status__      = "Development"
__module__      = ""

env_file.load('/srv/little_brother/.env')
dformat = '{date:%H:%M:%S} '


class handler(file_handler):
    formatters = {
        'privmsg': dformat + '<{mask.nick}> {data}',
        'join':    dformat + '*** {mask.nick} has joined {channel}',
        'part':    dformat + '*** {mask.nick} has left {channel} ({data})',
        'notice':  dformat + '{data}',
    }


@irc3.plugin
class MeetingManager():

    logfile = "/srv/little_brother/remind.log"

    def __init__(self, bot):
        self.bot = bot

    @command
    def remind(self, mask, target, args):
        """Remind about events
            %%remind [<text>...]
        """
        bot = self.bot
        if args['<text>']:
            text = ' '.join(args['<text>'])
            self.put_line(f"{text} ({mask.nick})")
            bot.notice(target, f'{text!r} added to message queue', nowait=True)
        else:  # User wants to see remind log
            for line in self.get_lines():
                bot.notice(target, line, nowait=True)
            if len(list(self.get_lines())) == 0:
                bot.notice(target, "*-* nothing to see here *-*", nowait=True)

    @command
    def remove(self, mask, target, args):
        """Remove reminders - Only the person who created the reminder can remove it
            %%remove [<text>...]
        """
        bot = self.bot
        if args['<text>']:
            text = ' '.join(args['<text>'])
        with open(self.logfile, "r") as fp:
            lines = fp.readlines()
        with open(self.logfile, "w") as fp:
            for line in lines:
                if line.strip("\n") != f"{text} ({mask.nick})":
                    fp.write(line)
                else:
                    bot.notice(target, f'{text!r} removed from the message queue', nowait=True)

    # Run the meeting playback at 16:00 UTC on Thursdays and Sundays
    @cron("0 16 * * 4,7")
    async def playback(self, target=None):
        bot = self.bot
        if target is None:
            target = bot.config["autojoins"][0]

        if target.startswith("#"):
            bot.notice(target, " : ".join(
                name for name in
                (await bot.async_cmds.names(target))['names']
                if name != bot.nick), nowait=True)

        for line in os.environ["WELCOME_MSG"].split('|'):
            bot.notice(target, line, nowait=True)
            await asyncio.sleep(2.5)

        for line in self.get_lines():
            bot.notice(target, line, nowait=True)
            await asyncio.sleep(0.5)

        if len(list(self.get_lines())) == 0:
            bot.notice(target, "*-* nothing to see here *-*", nowait=True)
            await asyncio.sleep(2.5)
            bot.notice(target, "Uh-oh! That was embarassing...", nowait=True)
            await asyncio.sleep(0.5)

        await asyncio.sleep(2.5)
        bot.notice(target, "Have a nice day!", nowait=True)

        self.clear_logfile()

    @command(show_in_help_list=False, permission='admin')
    async def cronforce(self, mask, target, args):
        """Force cron job playback
            %%cronforce
        """
        await self.playback()

    def get_lines(self):
        with open(self.logfile, "r") as fp:
            for line in fp:
                yield line.strip()

    def clear_logfile(self):
        with open(self.logfile, "w"):
            pass

    def put_line(self, line):
        with open(self.logfile, "a") as fp:
            fp.write(line + "\n")


@command
def time(bot, mask, target, args):
    """Get the time
        %%time
    """
    bot.privmsg(target, str(datetime.datetime.now()))


@command
def event(bot, mask, target, args):
    """Create event on novawebdevelopment.org
        %%event <arguments>...
        Syntax: !event <title>, <description>, <start_datetime>, <end_datetime>, <location>, <maximum_no_participants>
        Note: Accepted values for <maximum_no_participants> - infinity, 0, 1, 2, etc.
    """
    arguments_list = ' '.join(args['<arguments>'])
    arguments_list = arguments_list.split(", ")

    br = mechanize.Browser()
    br.open("https://novawebdevelopment.org/accounts/login/")

    br.form = list(br.forms())[0]
    br.form['email'] = os.environ["BIG_BROTHER_EMAIL"]
    br.form['password'] = os.environ["BIG_BROTHER_PASSWORD"]
#   response = br.submit()

    br.open("https://novawebdevelopment.org/events/create/")
    br.form = list(br.forms())[0]
    try:
        br.form['title'] = arguments_list[0]
        br.form['description'] = arguments_list[1]
        br.form['start_date'] = arguments_list[2]
        br.form['end_date'] = arguments_list[3]
        br.form['location'] = arguments_list[4]
        if arguments_list[5] == 'infinity':
            br.form['maximum_no_participants'] = '-1'
        else:
            br.form['maximum_no_participants'] = arguments_list[5]
#       response = br.submit()

        if br.geturl() == "https://novawebdevelopment.org/events/create/":
            bot.notice(target, 'The event has invalid arguments.', nowait=True)
        else:
            bot.notice(target,
                       f'The event "{arguments_list[0]}" has been created.', nowait=True)

    except IndexError:
        bot.notice(target, 'The event has missing arguments.', nowait=True)
